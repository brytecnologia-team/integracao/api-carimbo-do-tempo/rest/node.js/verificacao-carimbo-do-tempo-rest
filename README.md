# Verificação de Carimbo do Tempo

Este é um exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia JavaScript para verificação de carimbo do tempo. 

### Tech

O exemplo utiliza a biblioteca JavaScript abaixo:
* [Request] - Biblioteca para fazer chamadas http.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastra-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.


**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a verificação do carimbo do tempo.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| ACCESS_TOKEN | Access Token para o consumo do serviço (JWT). | ServiceConfig

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.


**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

   [Request]: <https://github.com/request/request>


<b>1</b> - As dependências podem ser instaladas utilizando o gerenciador de pacotes npm ou yarn.

Na pasta raiz do projeto, execute o comando de instalação conforme o gerenciador de dependências de sua preferência:

Instalando as dependências com npm:

    npm install

Instalando as dependências com yarn:

    yarn

<b>2</b> - Para executar a aplicação execute o comando:

    node src/main/app.js


