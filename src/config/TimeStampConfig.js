module.exports = {

    // Request identifier
    NONCE: 1,

    // Identifier of the time stamp within a batch
    NONCE_OF_TIME_STAMP: 1,

    // Available values: 'BASIC' = Report without certificate chain information and 'CHAIN' = Report with certificate chain information .
    MODE: 'CHAIN',

    // Available values: 'true' = Report with content of the certificate, chain e time stamp encoded in Base64 and 'false' = Report without content of certificate, chain and time stamp encoded in Base64
    CONTENT_RETURN: 'true',

    // location where the time stamp is stored
    TIME_STAMP_PATH: './arquivos/timestamp-teste1.tst',

    // location where the document is stored
    DOCUMENT_CONTENT_PATH: './arquivos/logoBRy.png',

    // hash of document content
    DOCUMENT_HASH: '2CAF281193A7580B88C0A15FE0C9AA7A6E940EA86BB0CC2917EF71229CA49393'
}