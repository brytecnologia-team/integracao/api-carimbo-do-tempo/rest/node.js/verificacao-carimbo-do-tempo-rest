const request = require('request');
const serviceConfig = require('../config/ServiceConfig');
const timeStampConfig = require('../config/TimeStampConfig');

var fs = require('fs');

const URL_TIME_STAMP_SERVER = serviceConfig.URL_TIME_STAMP_VERIFIER;

this.headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${serviceConfig.ACCESS_TOKEN}`
};

var auxAuthorization = this.headers.Authorization.split(' ');

const configuredToken = () => {
    if (auxAuthorization[1] === '<INSERT_VALID_ACCESS_TOKEN>') {
        console.log('Set up a valid token');
        return false;
    }
    return true;
}


const verifyBySendingOriginalDocument = (URL_TIME_STAMP_SERVER) => {

    const timeStampForm = {
        'nonce': timeStampConfig.NONCE,
        'mode': timeStampConfig.MODE,
        'contentReturn': timeStampConfig.CONTENT_RETURN,
        'timestamps[0][nonce]': timeStampConfig.NONCE_OF_TIME_STAMP,
        'timestamps[0][content]': fs.createReadStream(timeStampConfig.TIME_STAMP_PATH),
        'timestamps[0][documentContent]': fs.createReadStream(timeStampConfig.DOCUMENT_CONTENT_PATH)
    }

    return new Promise((resolve, reject) => {

        request.post({ url: URL_TIME_STAMP_SERVER, formData: timeStampForm, headers: this.headers }, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })

}

const verifyBySendingHashOfOriginalDocument = (URL_TIME_STAMP_SERVER) => {

    const timeStampForm = {
        'nonce': timeStampConfig.NONCE,
        'mode': timeStampConfig.MODE,
        'contentReturn': timeStampConfig.CONTENT_RETURN,
        'timestamps[0][nonce]': timeStampConfig.NONCE_OF_TIME_STAMP,
        'timestamps[0][content]': fs.createReadStream(timeStampConfig.TIME_STAMP_PATH),
        'timestamps[0][documentHash]': timeStampConfig.DOCUMENT_HASH
    }

    return new Promise((resolve, reject) => {

        request.post({ url: URL_TIME_STAMP_SERVER, formData: timeStampForm, headers: this.headers }, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })

}

const verify = (URL_TIME_STAMP_SERVER) => {

    const timeStampForm = {
        'nonce': timeStampConfig.NONCE,
        'mode': timeStampConfig.MODE,
        'contentReturn': timeStampConfig.CONTENT_RETURN,
        'timestamps[0][nonce]': timeStampConfig.NONCE_OF_TIME_STAMP,
        'timestamps[0][content]': fs.createReadStream(timeStampConfig.TIME_STAMP_PATH)
    }

    return new Promise((resolve, reject) => {

        request.post({ url: URL_TIME_STAMP_SERVER, formData: timeStampForm, headers: this.headers }, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })

}

function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}

module.exports = { URL_TIME_STAMP_SERVER, verifyBySendingOriginalDocument, verifyBySendingHashOfOriginalDocument, verify, configuredToken, sleep };