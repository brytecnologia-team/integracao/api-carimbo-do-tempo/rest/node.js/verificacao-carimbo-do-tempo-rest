const { URL_TIME_STAMP_SERVER, verifyBySendingOriginalDocument, verifyBySendingHashOfOriginalDocument, verify, configuredToken, sleep } = require('../functions/TimeStampVerifier');


function timeStampReport(timestamp) {
    console.log();

    console.log('Time stamp status: ', timestamp.timeStampStatus.status);

    console.log('Hash of stamped content: ', timestamp.timeStampStatus.timeStampContentHash);

    console.log('Time stamp date: ', timestamp.timeStampStatus.timeStampDate);

    console.log('Time stamp serial number: ', timestamp.timeStampStatus.timestampSerialNumber);

    console.log('Time stamp policy: ', timestamp.timeStampStatus.timeStampPolicy);
}

async function main() {
    if (configuredToken()) {

        console.log('================Starting time stamp verification by sending original document ... ================');

        verifyBySendingOriginalDocument(URL_TIME_STAMP_SERVER).then(result => {
            console.log('JSON response of time stamp verification: ', result);

            var timestamp = result.reports[0];

            timeStampReport(timestamp);

        }).catch(error => {
            console.log(error);
        })

        await sleep(2000);

        console.log();
        console.log('=============Starting time stamp verification by sending hash of the original document ... =============');

        verifyBySendingHashOfOriginalDocument(URL_TIME_STAMP_SERVER).then(result => {
            console.log('JSON response of time stamp verification: ', result);

            var timestamp = result.reports[0];

            timeStampReport(timestamp);

        }).catch(error => {
            console.log(error);
        })

        await sleep(2000);

        console.log();
        console.log('=============Starting time stamp verification without sending original document data ... =============');

        verify(URL_TIME_STAMP_SERVER).then(result => {
            console.log('JSON response of time stamp verification: ', result);

            var timestamp = result.reports[0];

            timeStampReport(timestamp);

        }).catch(error => {
            console.log(error);
        })

    }
}

main();